import base64
import pandas as pd
import seaborn as sns
import numpy as np
import nltk
import re
import xgboost as xgb
from nltk.corpus import stopwords
from sklearn import metrics
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, TfidfTransformer
from sklearn.decomposition import NMF, LatentDirichletAllocation
from sklearn.model_selection import StratifiedKFold, train_test_split, GridSearchCV
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.base import BaseEstimator, TransformerMixin
from matplotlib import pyplot as plt

eng_stopwords = set(stopwords.words("english"))

class Normalise(BaseEstimator, TransformerMixin):

    def fit(self, x, y=None):
        return self

    def transform(self, df):
        text = []
        for line in df:
            line = re.sub('[^a-zA-Z0-9]', ' ', line)
            line = line.lower()
            line = line.split()
            line = [word for word in line if word not in stopwords.words('english')]
            line = ' '.join(line)
            text.append(line)
        return text

class ExtractFeatures(BaseEstimator, TransformerMixin):
    """Extract features from each sentence for DictVectorizer"""

    def fit(self, x, y=None):
        return self

    def transform(self, df):
        return [{ 'length': len(text),
                'num_stopwords': len([w for w in str(text).lower().split() if w in eng_stopwords])}
            for text in df]

if __name__ == '__main__':

    train_df = pd.read_csv("data/train.csv", usecols=['text', 'author'])
    test_df = pd.read_csv("data/test.csv")

                
    def test_grid_pipeline(df, nlp_pipeline, parameters=None, pipeline_name=''):
        y = df['author'].copy()
        X = pd.Series(df['text'])
        kfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=1)
        grid_search = GridSearchCV(nlp_pipeline, parameters, scoring="neg_log_loss", n_jobs=-1, cv=kfold, verbose=2)
        X_train, X_valid, y_train, y_valid = train_test_split(
            X, y, stratify=y, test_size=0.25, random_state=80)
        
        grid_result = grid_search.fit(X_train, y_train)

        # summarize results
        print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
        means = grid_result.cv_results_['mean_test_score']
        stds = grid_result.cv_results_['std_test_score']
        params = grid_result.cv_results_['params']
        for mean, stdev, param in zip(means, stds, params):
            print("%f (%f) with: %r" % (mean, stdev, param))    

    union_pipeline = Pipeline([
        # Use FeatureUnion to combine the features from subject and body
        ('union', FeatureUnion(
            transformer_list=[

                # Misc features
                ('feats', Pipeline([
                    ('stats', ExtractFeatures()),  # returns a list of dicts
                    ('vect', DictVectorizer()),  # list of dicts -> feature matrix
                ])),

                ('text', Pipeline([
                    # ('norm', Normalise()),
                    ('cv', CountVectorizer(ngram_range=(1, 2))),
                    ('tfidf', TfidfTransformer(use_idf=False))
                ]))
            ],

            # weight components in FeatureUnion
            transformer_weights={
                'text': 1,
                'feats': 1
            },
        )),

        ('clf', MultinomialNB(alpha=0.01))
    ])

    # parameters = {'union__text': [0, 0.25, 0.5, 0.75, 1],
    #               'union__feats': [0, 0.25, 0.5, 0.75, 1],
    # }
    test_grid_pipeline(train_df, union_pipeline, parameters=parameters)


    