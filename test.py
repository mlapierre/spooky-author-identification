import base64
import pandas as pd
import seaborn as sns
import nltk
from sklearn import metrics
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation
from sklearn.model_selection import StratifiedKFold, GridSearchCV, train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import LabelEncoder
from matplotlib import pyplot as plt

if __name__ == '__main__':

    train_df = pd.read_csv("data/train.csv", usecols=['text', 'author'])
    test_df = pd.read_csv("data/test.csv", usecols=['text'])


    def test_pipeline(df, nlp_pipeline, parameters=None, pipeline_name=''):
        y = df['author'].copy()
        X = pd.Series(df['text'])
        kfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=1)
        grid_search = GridSearchCV(nlp_pipeline, parameters, scoring="neg_log_loss", n_jobs=-1, cv=kfold, verbose=2)
        X_train, X_valid, y_train, y_valid = train_test_split(
            X, y, stratify=y, test_size=0.25, random_state=80)
        
        grid_result = grid_search.fit(X_train, y_train)

        # summarize results
        print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
        means = grid_result.cv_results_['mean_test_score']
        stds = grid_result.cv_results_['std_test_score']
        params = grid_result.cv_results_['params']
        for mean, stdev, param in zip(means, stds, params):
            print("%f (%f) with: %r" % (mean, stdev, param))        

    # pipe = Pipeline([
    #     ('tfidf', TfidfVectorizer(strip_accents='unicode', analyzer='word',
    #             stop_words='english', sublinear_tf=True, min_df=3, ngram_range=(1, 3))),
    #     ('mnb', MultinomialNB())
    # ])

    # unigram_pipe = Pipeline([
    #     ('cv', CountVectorizer()),
    #     ('mnb', MultinomialNB())
    # ])
    # Best: -0.494727 using {'cv__max_df': 1.0, 'cv__min_df': 1, 'cv__ngram_range': (1, 1)} 

    parameters = {
        'mnb__alpha': [0.001, 0.01, 0.1, 1, 10, 100]
    }

    pipe = Pipeline([
        ('cv', CountVectorizer()),
        ('mnb', MultinomialNB())
    ])

    test_pipeline(train_df, pipe, parameters=parameters)


    