{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "# Feature Union with Heterogeneous Data Sources\n",
    "\n",
    "\n",
    "Datasets can often contain components of that require different feature\n",
    "extraction and processing pipelines.  This scenario might occur when:\n",
    "\n",
    "1. Your dataset consists of heterogeneous data types (e.g. raster images and\n",
    "   text captions)\n",
    "2. Your dataset is stored in a Pandas DataFrame and different columns\n",
    "   require different processing pipelines.\n",
    "\n",
    "This example demonstrates how to use\n",
    ":class:`sklearn.feature_extraction.FeatureUnion` on a dataset containing\n",
    "different types of features.  We use the 20-newsgroups dataset and compute\n",
    "standard bag-of-words features for the subject line and body in separate\n",
    "pipelines as well as ad hoc features on the body. We combine them (with\n",
    "weights) using a FeatureUnion and finally train a classifier on the combined\n",
    "set of features.\n",
    "\n",
    "The choice of features is not particularly helpful, but serves to illustrate\n",
    "the technique.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Author: Matt Terry <matt.terry@gmail.com>\n",
    "#\n",
    "# License: BSD 3 clause\n",
    "from __future__ import print_function\n",
    "\n",
    "import numpy as np\n",
    "\n",
    "from sklearn.base import BaseEstimator, TransformerMixin\n",
    "from sklearn.datasets import fetch_20newsgroups\n",
    "from sklearn.datasets.twenty_newsgroups import strip_newsgroup_footer\n",
    "from sklearn.datasets.twenty_newsgroups import strip_newsgroup_quoting\n",
    "from sklearn.decomposition import TruncatedSVD\n",
    "from sklearn.feature_extraction import DictVectorizer\n",
    "from sklearn.feature_extraction.text import TfidfVectorizer\n",
    "from sklearn.metrics import classification_report\n",
    "from sklearn.pipeline import FeatureUnion\n",
    "from sklearn.pipeline import Pipeline\n",
    "from sklearn.svm import SVC"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "class ItemSelector(BaseEstimator, TransformerMixin):\n",
    "    \"\"\"For data grouped by feature, select subset of data at a provided key.\n",
    "\n",
    "    The data is expected to be stored in a 2D data structure, where the first\n",
    "    index is over features and the second is over samples.  i.e.\n",
    "\n",
    "    >> len(data[key]) == n_samples\n",
    "\n",
    "    Please note that this is the opposite convention to scikit-learn feature\n",
    "    matrixes (where the first index corresponds to sample).\n",
    "\n",
    "    ItemSelector only requires that the collection implement getitem\n",
    "    (data[key]).  Examples include: a dict of lists, 2D numpy array, Pandas\n",
    "    DataFrame, numpy record array, etc.\n",
    "\n",
    "    >> data = {'a': [1, 5, 2, 5, 2, 8],\n",
    "               'b': [9, 4, 1, 4, 1, 3]}\n",
    "    >> ds = ItemSelector(key='a')\n",
    "    >> data['a'] == ds.transform(data)\n",
    "\n",
    "    ItemSelector is not designed to handle data grouped by sample.  (e.g. a\n",
    "    list of dicts).  If your data is structured this way, consider a\n",
    "    transformer along the lines of `sklearn.feature_extraction.DictVectorizer`.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    key : hashable, required\n",
    "        The key corresponding to the desired value in a mappable.\n",
    "    \"\"\"\n",
    "    def __init__(self, key):\n",
    "        self.key = key\n",
    "\n",
    "    def fit(self, x, y=None):\n",
    "        return self\n",
    "\n",
    "    def transform(self, data_dict):\n",
    "        return data_dict[self.key]\n",
    "\n",
    "\n",
    "class TextStats(BaseEstimator, TransformerMixin):\n",
    "    \"\"\"Extract features from each document for DictVectorizer\"\"\"\n",
    "\n",
    "    def fit(self, x, y=None):\n",
    "        return self\n",
    "\n",
    "    def transform(self, posts):\n",
    "        return [{'length': len(text),\n",
    "                 'num_sentences': text.count('.')}\n",
    "                for text in posts]\n",
    "\n",
    "\n",
    "class SubjectBodyExtractor(BaseEstimator, TransformerMixin):\n",
    "    \"\"\"Extract the subject & body from a usenet post in a single pass.\n",
    "\n",
    "    Takes a sequence of strings and produces a dict of sequences.  Keys are\n",
    "    `subject` and `body`.\n",
    "    \"\"\"\n",
    "    def fit(self, x, y=None):\n",
    "        return self\n",
    "\n",
    "    def transform(self, posts):\n",
    "        features = np.recarray(shape=(len(posts),),\n",
    "                               dtype=[('subject', object), ('body', object)])\n",
    "        for i, text in enumerate(posts):\n",
    "            headers, _, bod = text.partition('\\n\\n')\n",
    "            bod = strip_newsgroup_footer(bod)\n",
    "            bod = strip_newsgroup_quoting(bod)\n",
    "            features['body'][i] = bod\n",
    "\n",
    "            prefix = 'Subject:'\n",
    "            sub = ''\n",
    "            for line in headers.split('\\n'):\n",
    "                if line.startswith(prefix):\n",
    "                    sub = line[len(prefix):]\n",
    "                    break\n",
    "            features['subject'][i] = sub\n",
    "\n",
    "        return features\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "pipeline = Pipeline([\n",
    "    # Extract the subject & body\n",
    "    ('subjectbody', SubjectBodyExtractor()),\n",
    "\n",
    "    # Use FeatureUnion to combine the features from subject and body\n",
    "    ('union', FeatureUnion(\n",
    "        transformer_list=[\n",
    "\n",
    "            # Pipeline for pulling features from the post's subject line\n",
    "            ('subject', Pipeline([\n",
    "                ('selector', ItemSelector(key='subject')),\n",
    "                ('tfidf', TfidfVectorizer(min_df=50)),\n",
    "            ])),\n",
    "\n",
    "            # Pipeline for standard bag-of-words model for body\n",
    "            ('body_bow', Pipeline([\n",
    "                ('selector', ItemSelector(key='body')),\n",
    "                ('tfidf', TfidfVectorizer()),\n",
    "                ('best', TruncatedSVD(n_components=50)),\n",
    "            ])),\n",
    "\n",
    "            # Pipeline for pulling ad hoc features from post's body\n",
    "            ('body_stats', Pipeline([\n",
    "                ('selector', ItemSelector(key='body')),\n",
    "                ('stats', TextStats()),  # returns a list of dicts\n",
    "                ('vect', DictVectorizer()),  # list of dicts -> feature matrix\n",
    "            ])),\n",
    "\n",
    "        ],\n",
    "\n",
    "        # weight components in FeatureUnion\n",
    "        transformer_weights={\n",
    "            'subject': 0.8,\n",
    "            'body_bow': 0.5,\n",
    "            'body_stats': 1.0,\n",
    "        },\n",
    "    )),\n",
    "\n",
    "    # Use a SVC classifier on the combined features\n",
    "    ('svc', SVC(kernel='linear')),\n",
    "])\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Downloading 20news dataset. This may take a few minutes.\n",
      "Downloading dataset from https://ndownloader.figshare.com/files/5975967 (14 MB)\n"
     ]
    }
   ],
   "source": [
    "# limit the list of categories to make running this example faster.\n",
    "categories = ['alt.atheism', 'talk.religion.misc']\n",
    "train = fetch_20newsgroups(random_state=1,\n",
    "                           subset='train',\n",
    "                           categories=categories,\n",
    "                           )\n",
    "test = fetch_20newsgroups(random_state=1,\n",
    "                          subset='test',\n",
    "                          categories=categories,\n",
    "                          )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "             precision    recall  f1-score   support\n",
      "\n",
      "          0       0.59      0.80      0.68       234\n",
      "          1       0.81      0.61      0.70       336\n",
      "\n",
      "avg / total       0.72      0.69      0.69       570\n",
      "\n"
     ]
    }
   ],
   "source": [
    "pipeline.fit(train.data, train.target)\n",
    "y = pipeline.predict(test.data)\n",
    "print(classification_report(y, test.target))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['From: karner@austin.ibm.com (F. Karner)\\nSubject: Re: Jews can\\'t hide from keith@cco.\\nOriginator: frank@karner.austin.ibm.com\\nOrganization: IBM Advanced Workstation Division\\nLines: 50\\n\\n\\nIn article <1pj2b6$aaa@fido.asd.sgi.com>, livesey@solntze.wpd.sgi.com (Jon Livesey) writes:\\n> In article <1993Apr3.033446.10669@bmerh85.bnr.ca>, dgraham@bmers30.bnr.ca (Douglas Graham) writes:\\n> |> In article <1pint5$1l4@fido.asd.sgi.com> livesey@solntze.wpd.sgi.com (Jon Livesey) writes:\\n> |> >\\n> |> Deletions...\\n> |> Er, Jon, what Ken said was:\\n> |> \\n> |>   There have previously been people like you in your country.  Unfortunately,\\n> |>                              ^^^^^^^^^^^^^^^\\n> |>   most Jews did not survive.\\n> |> \\n> |> That sure sounds to me like Ken is accusing the guy of being a Nazi.\\n> \\n> Hitler and the Nazis didn\\'t spring fully formed from the forehead\\n> of Athena.   They didn\\'t invent anti-semitism.   They built on a \\n> foundation of anti-semitism that was already present in Germany.   \\n> This foundation of anti-semitism was laid down, not by the Nazis, \\n> but by the people I listed, and also by hundreds of years of unthinking, \\n> knee-jerk bigotry, on the part of perfectly ordinary people, and, of\\n> course, their pastors and priests.\\n> \\n> What we have to worry about today is not whether some Hollywood\\n> Hitler in a black uniform is going to come striding onto the German\\n> stage in one unprepared step, but whether those same bedrock foundations\\n> of anti-semitism are being laid down, little by little, in Germany,\\n> as we speak.\\n> \\n> And if so, they will be laid down, not by Hitlers and Himmlers, who\\n> will come later, but by \"people like\" the poster in question.   The\\n> people who think that casual anti-semitism is acceptable, or even fun.\\n>                       ^^^^^^^^^^^^^^^^^^^^\\n> \\nDeletions...\\n> I did.     Now may I suggest, with the greatest possible respect, that\\n> you go read some history?\\n> \\n> jon.\\n\\nSo, you consider the german poster\\'s remark anti-semitic?  Perhaps you\\nimply that anyone in Germany who doesn\\'t agree with israely policy in a\\nnazi?  Pray tell, how does it even qualify as \"casual anti-semitism\"? \\nIf the term doesn\\'t apply, why then bring it up?\\n\\nYour own bigotry is shining through.  \\n-- \\n\\n         DISCLAIMER: The opinions expressed in this posting are mine\\n            solely and do not represent my employer in any way.\\n       F. A. Karner AIX Technical Support | karner@austin.vnet.ibm.com\\n']\n"
     ]
    }
   ],
   "source": [
    "print(train.data[:1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "rec.array([ (\" Re: Jews can't hide from keith@cco.\", 'Deletions...\\n\\nSo, you consider the german poster\\'s remark anti-semitic?  Perhaps you\\nimply that anyone in Germany who doesn\\'t agree with israely policy in a\\nnazi?  Pray tell, how does it even qualify as \"casual anti-semitism\"? \\nIf the term doesn\\'t apply, why then bring it up?\\n\\nYour own bigotry is shining through.  \\n-- '),\n",
      " (' Re: <<Pompous ass', '\\n  If the Anne Frank exhibit makes it to your small little world,\\n  take an afternoon to go see it.  \\n\\n\\n/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\/\\\\ \\n\\nBob Beauchaine bobbe@vice.ICO.TEK.COM \\n\\nThey said that Queens could stay, they blew the Bronx away,\\nand sank Manhattan out at sea.'),\n",
      " (' Re: Catholic Lit-Crit of a.s.s.', '\\n\\n(Pleading mode on)\\n\\nPlease!  I\\'m begging you!  Quit confusing religious groups, and stop\\nmaking generalizations!  I\\'m a Protestant!  I\\'m an evangelical!  I don\\'t\\nbelieve that my way is the only way!  I\\'m not a \"creation scientist\"!  I\\ndon\\'t think that homosexuals should be hung by their toenails!  \\n\\nIf you want to discuss bible thumpers, you would be better off singling\\nout (and making obtuse generalizations about) Fundamentalists.  If you\\ncompared the actions of Presbyterians or Methodists with those of Southern \\nBaptists, you would think that they were different religions!\\n\\nPlease, prejudice is about thinking that all people of a group are the\\nsame, so please don\\'t write off all Protestants or all evangelicals!\\n\\n(Pleading mode off.)\\n\\nGod.......I wish I could get ahold of all the Thomas Stories......')], \n",
      "          dtype=[('subject', 'O'), ('body', 'O')])\n"
     ]
    }
   ],
   "source": [
    "posts = train.data[:3]\n",
    "features = np.recarray(shape=(len(posts),),\n",
    "                       dtype=[('subject', object), ('body', object)])\n",
    "for i, text in enumerate(posts):\n",
    "    headers, _, bod = text.partition('\\n\\n')\n",
    "    bod = strip_newsgroup_footer(bod)\n",
    "    bod = strip_newsgroup_quoting(bod)\n",
    "    features['body'][i] = bod\n",
    "\n",
    "    prefix = 'Subject:'\n",
    "    sub = ''\n",
    "    for line in headers.split('\\n'):\n",
    "        if line.startswith(prefix):\n",
    "            sub = line[len(prefix):]\n",
    "            break\n",
    "    features['subject'][i] = sub\n",
    "    \n",
    "from pprint import pprint\n",
    "pprint(features)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([\" Re: Jews can't hide from keith@cco.\", ' Re: <<Pompous ass'], dtype=object)"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "features.subject"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[{'length': 320, 'num_sentences': 4}, {'length': 305, 'num_sentences': 5}]"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "feat_dict = [{'length': len(text),\n",
    "  'num_sentences': text.count('.')}\n",
    "    for text in features.body]\n",
    "feat_dict"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<2x2 sparse matrix of type '<class 'numpy.float64'>'\n",
       "\twith 4 stored elements in Compressed Sparse Row format>"
      ]
     },
     "execution_count": 37,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "dv = DictVectorizer()\n",
    "dvt = dv.fit_transform(feat_dict)\n",
    "dvt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['length', 'num_sentences']"
      ]
     },
     "execution_count": 35,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "dv.get_feature_names()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ 320.,    4.],\n",
       "       [ 305.,    5.]])"
      ]
     },
     "execution_count": 38,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "dvt.toarray()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[{'length': 320}, {'length': 305}]"
      ]
     },
     "execution_count": 42,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "len_feat = [{'length': len(text)} for text in features.body]\n",
    "len_feat"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ 320.],\n",
       "       [ 305.]])"
      ]
     },
     "execution_count": 45,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "dv1 = DictVectorizer()\n",
    "dv1t = dv.fit_transform(len_feat)\n",
    "dv1t.toarray()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[{'num_sentences': 4}, {'num_sentences': 5}]"
      ]
     },
     "execution_count": 46,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "num_sent_feat = [{'num_sentences': text.count('.')} for text in features.body]\n",
    "num_sent_feat"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 48,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ 4.],\n",
       "       [ 5.]])"
      ]
     },
     "execution_count": 48,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "dv2 = DictVectorizer()\n",
    "dv2t = dv.fit_transform(num_sent_feat)\n",
    "dv2t.toarray()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ 320.,    4.,  320.,    4.],\n",
       "       [ 305.,    5.,  305.,    5.],\n",
       "       [ 814.,   15.,  814.,   15.]])"
      ]
     },
     "execution_count": 55,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "feat_dict = [{'length': len(text),\n",
    "  'num_sentences': text.count('.')}\n",
    "    for text in features.body]\n",
    "\n",
    "union = FeatureUnion([('len', dv1), ('num_sent', dv2)])\n",
    "uft = union.fit_transform(feat_dict)\n",
    "uft.toarray()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
