# In[ ]:

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

# In[ ]:

train_df = pd.read_csv("data/train.csv", usecols=['text', 'author'])
test_df = pd.read_csv("data/test.csv", usecols=['text'])

# In[ ]:

# Display a small sample of each author's data
grouped_df = train_df.groupby('author')
for name, group in grouped_df:
    print("Author name : ", name)
    sample = group.sample(5)
    for i, row in sample.iterrows():
        print('{0}: {1}'.format(i, row['text']))
    print('\n')

# In[ ]:

# Let's have a look at the distribution of line lengths
sns.set()
plt.style.use('seaborn-whitegrid')
lengths = [len(line) for line in train_df['text']]
plt.hist(lengths, bins=np.arange(max(lengths)), histtype='step', linewidth=1)

# That long tail is not suprising, although there seem to be a few very very long lines!

# In[]

# Let's have a look a the longest
print(max(lengths))

# 
maxline = [line for line in train_df['text'] if len(line) == max(lengths)][0].split()
n = 10
for i in range(0,len(maxline),n):
    print(' '.join(maxline[i:i+n]))

# A quick search of the original text reveals that our dataset is missing some 
# punctuation; em dashes and ampersands.

# In[]
for name, group in grouped_df:
    lengths = [len(line) for line in group['text']]
    plt.hist(lengths, bins=np.arange(600), histtype='step', linewidth=1, label=name)
plt.legend()
