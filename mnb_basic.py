# coding: utf-8

# In[ ]:

# Imports
import base64
import pandas as pd
import seaborn as sns
import numpy as np
import nltk
from nltk.corpus import stopwords
from sklearn import metrics
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation
from sklearn.model_selection import StratifiedKFold, train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.base import BaseEstimator, TransformerMixin
from matplotlib import pyplot as plt
#get_ipython().run_line_magic('matplotlib', 'inline')

# In[ ]:

# Load the data with Pandas
train_df = pd.read_csv("data/train.csv", usecols=['text', 'author'])
test_df = pd.read_csv("data/test.csv")

# In[ ]:

def test_pipeline(df, nlp_pipeline, pipeline_name=''):
    y = df['author'].copy()
    X = pd.Series(df['text'])
    rskf = StratifiedKFold(n_splits=5, random_state=1)
    losses = []
    for train_index, test_index in rskf.split(X, y):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        nlp_pipeline.fit(X_train, y_train)
        losses.append(metrics.log_loss(y_test, nlp_pipeline.predict_proba(X_test)))
    print(f'{pipeline_name} kfolds log losses: {str([str(round(x, 3)) for x in sorted(losses)])}')
    print(f'{pipeline_name} mean log loss: {round(pd.np.mean(losses), 3)}')

# In[ ]:
eng_stopwords = set(stopwords.words("english"))

class ExtractFeatures(BaseEstimator, TransformerMixin):
    """Extract features from each sentence for DictVectorizer"""

    def fit(self, x, y=None):
        return self

    def transform(self, posts):
        return [{ 'length': len(text),
                  'num_stopwords': len([w for w in str(text).lower().split() if w in eng_stopwords])}
            for text in posts]

# In[ ]:

union_pipeline = Pipeline([
    # Use FeatureUnion to combine the features from subject and body
    ('union', FeatureUnion(
        transformer_list=[

            # Bag of words
            ('cv', CountVectorizer()),

            # Misc features
            ('feats', Pipeline([
                ('stats', ExtractFeatures()),  # returns a list of dicts
                ('vect', DictVectorizer()),  # list of dicts -> feature matrix
            ])),

        ],

        # weight components in FeatureUnion
        transformer_weights={
            'cv': 1,
            'feats': 1
        },
    )),

    ('mnb', MultinomialNB()),
])
test_pipeline(train_df, union_pipeline, "Feature union pipeline")

#Feature union pipeline kfolds log losses: ['0.459', '0.462', '0.472', '0.476', '0.476']
#Feature union pipeline mean log loss: 0.469

# In[ ]:
union_pipeline.fit(train_df.text, train_df.author)
pred_df = pd.DataFrame(union_pipeline.predict_proba(test_df.text),
                       columns=union_pipeline.classes_)
pred_df.insert(0, column='id', value=test_df['id'])
pred_df.head()
pred_df.to_csv("submission.csv", index=False)