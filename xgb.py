# coding: utf-8

# In[ ]:

# Imports
import base64
import pandas as pd
import seaborn as sns
import numpy as np
import nltk
import re
import xgboost as xgb
from nltk.corpus import stopwords
from sklearn import metrics
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, TfidfTransformer
from sklearn.decomposition import NMF, LatentDirichletAllocation
from sklearn.model_selection import StratifiedKFold, train_test_split, GridSearchCV
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.base import BaseEstimator, TransformerMixin
from matplotlib import pyplot as plt
#get_ipython().run_line_magic('matplotlib', 'inline')

# In[ ]:

# Load the data with Pandas
train_df = pd.read_csv("data/train.csv", usecols=['text', 'author'])
test_df = pd.read_csv("data/test.csv")

# In[ ]:

def test_pipeline(df, nlp_pipeline, pipeline_name=''):
    y = df['author'].copy()
    X = pd.Series(df['text'])
    rskf = StratifiedKFold(n_splits=5, random_state=1)
    losses = []
    for train_index, test_index in rskf.split(X, y):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        nlp_pipeline.fit(X_train, y_train)
        losses.append(metrics.log_loss(y_test, nlp_pipeline.predict_proba(X_test)))
    print(f'{pipeline_name} kfolds log losses: {str([str(round(x, 3)) for x in sorted(losses)])}')
    print(f'{pipeline_name} mean log loss: {round(pd.np.mean(losses), 3)}')



def test_grid_pipeline(df, nlp_pipeline, parameters=None, pipeline_name=''):
    y = df['author'].copy()
    X = pd.Series(df['text'])
    kfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=1)
    grid_search = GridSearchCV(nlp_pipeline, parameters, scoring="neg_log_loss", n_jobs=-1, cv=kfold, verbose=2)
    X_train, X_valid, y_train, y_valid = train_test_split(
        X, y, stratify=y, test_size=0.25, random_state=80)
    
    grid_result = grid_search.fit(X_train, y_train)

    # summarize results
    print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
    means = grid_result.cv_results_['mean_test_score']
    stds = grid_result.cv_results_['std_test_score']
    params = grid_result.cv_results_['params']
    for mean, stdev, param in zip(means, stds, params):
        print("%f (%f) with: %r" % (mean, stdev, param))        

# In[ ]:


unigram_pipe = Pipeline([
    ('cv', CountVectorizer()),
    ('mnb', MultinomialNB())
])
test_pipeline(train_df, unigram_pipe, "Unigrams only")

# Unigrams only kfolds log losses: ['0.455', '0.46', '0.47', '0.473', '0.474']
# Unigrams only mean log loss: 0.466


# In[ ]:
# Normalise text
text = train_df.text[0]
text = re.sub('[^a-zA-Z0-9]', ' ', text)
text = text.lower()
text = text.split()
text = [word for word in text if word not in stopwords.words('english')]
text = ' '.join(text)
text

# In[ ]:
eng_stopwords = set(stopwords.words("english"))
class Normalise(BaseEstimator, TransformerMixin):

    def fit(self, x, y=None):
        return self

    def transform(self, df):
        text = []
        for line in df:
            line = re.sub('[^a-zA-Z0-9]', ' ', line)
            line = line.lower()
            line = line.split()
            line = [word for word in line if word not in stopwords.words('english')]
            line = ' '.join(line)
            text.append(line)
        return text

class ExtractFeatures(BaseEstimator, TransformerMixin):
    """Extract features from each sentence for DictVectorizer"""

    def fit(self, x, y=None):
        return self

    def transform(self, df):
        return [{ 'length': len(text),
                  'num_stopwords': len([w for w in str(text).lower().split() if w in eng_stopwords])}
            for text in df]

# In[ ]:

union_pipeline = Pipeline([
    # Use FeatureUnion to combine the features from subject and body
    ('union', FeatureUnion(
        transformer_list=[

            # Misc features
            ('feats', Pipeline([
                ('stats', ExtractFeatures()),  # returns a list of dicts
                ('vect', DictVectorizer()),  # list of dicts -> feature matrix
            ])),

            ('text', Pipeline([
                # ('norm', Normalise()),
                ('cv', CountVectorizer(ngram_range=(1, 2))),
                ('tfidf', TfidfTransformer(use_idf=False))
            ]))
            

        ],

        # weight components in FeatureUnion
        transformer_weights={
            'text': 1,
            'feats': 1
        },
    )),

    ('clf', MultinomialNB(alpha=0.01))
    # ('xgb', xgb.XGBClassifier(max_depth=7, n_estimators=200, colsample_bytree=0.8, 
    #                     subsample=0.8, nthread=10, learning_rate=0.1, silent=False))
])

# In[ ]:
test_pipeline(train_df, union_pipeline, "Feature union pipeline")

# Feature union pipeline kfolds log losses: ['0.455', '0.46', '0.47', '0.473', '0.474']
# Feature union pipeline mean log loss: 0.466

#Feature union pipeline kfolds log losses: ['0.459', '0.462', '0.472', '0.476', '0.476']
#Feature union pipeline mean log loss: 0.469
# In[ ]:
union_pipeline.get_params().keys()

parameters = {'union__text__cv__ngram_range': [(1, 1), (1, 2), (1, 3)],
              'union__text__tfidf__use_idf': (True, False),
              'clf__alpha': (0, 0.01, 0.05, 0.1, 0.3, 0.5),
}
test_grid_pipeline(train_df, union_pipeline, parameters=parameters)


# In[ ]:
union_pipeline.fit(train_df.text, train_df.author)
pred_df = pd.DataFrame(union_pipeline.predict_proba(test_df.text),
                       columns=union_pipeline.classes_)
pred_df.insert(0, column='id', value=test_df['id'])
pred_df.head()
pred_df.to_csv("submission.csv", index=False)