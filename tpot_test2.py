import numpy as np

from tpot import TPOTClassifier
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split

mnist_data = load_digits()
training_features, testing_features, training_target, testing_target = \
    train_test_split(mnist_data.data.astype(np.float64), mnist_data.target.astype(np.float64), random_state=42)

tpot_obj = TPOTClassifier(
    random_state=42,
    population_size=150,
    offspring_size=2,
    generations=2,
    verbosity=3,
    config_dict='TPOT light',
    periodic_checkpoint_folder='./tmp'
)
tpot_obj.fit(training_features, training_target)
