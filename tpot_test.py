import pandas as pd

from tpot import TPOTClassifier
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import LabelEncoder

if __name__ == '__main__':

    train_df = pd.read_csv("data/train.csv", usecols=['text', 'author'])
    test_df = pd.read_csv("data/test.csv", usecols=['text'])

    lbl_enc = LabelEncoder()
    y = lbl_enc.fit_transform(train_df.author.values)

    #y = train_df['author'].copy()
    X = pd.Series(train_df['text'])

    X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, train_size=0.75, test_size=0.25, random_state=80)

    # pipeline_optimizer = TPOTClassifier(generations=5, population_size=20, cv=5,
    #                                     random_state=42, verbosity=2, scoring='neg_log_loss')
    # pipeline_optimizer.fit(X_train, y_train)
    # print(pipeline_optimizer.score(X_test, y_test))
    # pipeline_optimizer.export('tpot_exported_pipeline.py')

    # pipeline_optimizer =  Pipeline([
    #     ('cv', CountVectorizer()),
    #     ('tpot', TPOTClassifier(generations=2, population_size=3, cv=5, config_dict='TPOT sparse',
    #                 random_state=42, verbosity=2, scoring='neg_log_loss'))
    # ])

    cv = CountVectorizer()
    X_cvt = cv.fit_transform(X_train)

    pipeline_optimizer = TPOTClassifier(generations=2, population_size=100, n_jobs=2, 
                    config_dict='TPOT sparse', periodic_checkpoint_folder='./checkpoints',
                    random_state=42, verbosity=3, scoring='neg_log_loss')
    pipeline_optimizer.fit(X_cvt, y_train)

    #X_cvt_test = cv.fit_transform(X_test)
    #print(pipeline_optimizer.score(X_cvt_test, y_test))

    pipeline_optimizer.export('tpot_exported_pipeline.py')