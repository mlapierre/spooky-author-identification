# coding: utf-8

# In[ ]:

# Imports
import base64
import pandas as pd
import seaborn as sns
import numpy as np
import nltk
import re
import xgboost as xgb
from nltk.corpus import stopwords
from sklearn import metrics
from sklearn.calibration import CalibratedClassifierCV
from sklearn.ensemble import VotingClassifier, RandomForestClassifier
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, TfidfTransformer
from sklearn.decomposition import NMF, LatentDirichletAllocation
from sklearn.model_selection import StratifiedKFold, train_test_split, GridSearchCV
from sklearn.naive_bayes import MultinomialNB, BernoulliNB, GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.base import BaseEstimator, TransformerMixin
from matplotlib import pyplot as plt
#get_ipython().run_line_magic('matplotlib', 'inline')

# In[ ]:

# Load the data with Pandas
train_df = pd.read_csv("data/train.csv", usecols=['text', 'author'])
test_df = pd.read_csv("data/test.csv")

# In[ ]:

def test_pipeline(df, nlp_pipeline, pipeline_name=''):
    y = df['author'].copy()
    X = pd.Series(df['text'])
    rskf = StratifiedKFold(n_splits=5, random_state=1)
    losses = []
    for train_index, test_index in rskf.split(X, y):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        nlp_pipeline.fit(X_train, y_train)
        losses.append(metrics.log_loss(y_test, nlp_pipeline.predict_proba(X_test)))
    print(f'{pipeline_name} kfolds log losses: {str([str(round(x, 3)) for x in sorted(losses)])}')
    print(f'{pipeline_name} mean log loss: {round(pd.np.mean(losses), 3)}')

def test_grid_pipeline(df, nlp_pipeline, parameters=None, pipeline_name=''):
    y = df['author'].copy()
    X = pd.Series(df['text'])
    kfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=1)
    grid_search = GridSearchCV(nlp_pipeline, parameters, scoring="neg_log_loss", n_jobs=3, cv=kfold, verbose=2)
    X_train, X_valid, y_train, y_valid = train_test_split(
        X, y, stratify=y, test_size=0.25, random_state=80)
    
    grid_result = grid_search.fit(X_train, y_train)

    # summarize results
    means = grid_result.cv_results_['mean_test_score']
    stds = grid_result.cv_results_['std_test_score']
    params = grid_result.cv_results_['params']
    for mean, stdev, param in zip(means, stds, params):
        print("%f (%f) with: %r" % (mean, stdev, param))        
    print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))



#Feature union pipeline kfolds log losses: ['0.459', '0.462', '0.472', '0.476', '0.476']
#Feature union pipeline mean log loss: 0.469

# In[]:


if __name__ == '__main__':

    pipe = Pipeline([
        # ('cv', CountVectorizer(ngram_range=(1, 2))),
        # ('tfidf', TfidfTransformer(use_idf=False)),
        ('tfidf', TfidfVectorizer(token_pattern=r'\w{1,}', sublinear_tf=True, ngram_range=(1, 2))),
        ('rfc', RandomForestClassifier(n_jobs=4, verbose=1, random_state=1)),
    ])

    params = {
        'rfc__n_estimators': [50],
        'rfc__max_features': [0.10, 'auto'],
        'rfc__oob_score': [True, False],
        'rfc__warm_start': [True, False],
        #'rfc__max_features': ['auto', 'sqrt', 'log2', None, 0.10, 0.25, 0.5, 0.75]
    }
    #Best: -0.878387 using {'rfc__max_features': 0.1, 'rfc__n_estimators': 100}

    #print(pipe.get_params().keys())
    test_grid_pipeline(train_df, pipe, parameters=params)
    #test_pipeline(train_df, pipe)