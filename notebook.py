# coding: utf-8

# In[ ]:

# Imports
import base64
import pandas as pd
import seaborn as sns
import numpy as np
import nltk
from nltk.corpus import stopwords
from sklearn import metrics
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation
from sklearn.model_selection import StratifiedKFold, train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from matplotlib import pyplot as plt
#get_ipython().run_line_magic('matplotlib', 'inline')

# In[ ]:

# Load the data with Pandas
train_df = pd.read_csv("data/train.csv", usecols=['text', 'author'])
test_df = pd.read_csv("data/test.csv")

# In[ ]:

def test_pipeline(df, nlp_pipeline, pipeline_name=''):
    y = df['author'].copy()
    X = pd.Series(df['text'])
    rskf = StratifiedKFold(n_splits=5, random_state=1)
    losses = []
    for train_index, test_index in rskf.split(X, y):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        nlp_pipeline.fit(X_train, y_train)
        losses.append(metrics.log_loss(y_test, nlp_pipeline.predict_proba(X_test)))
    print(f'{pipeline_name} kfolds log losses: {str([str(round(x, 3)) for x in sorted(losses)])}')
    print(f'{pipeline_name} mean log loss: {round(pd.np.mean(losses), 3)}')

# In[ ]:


unigram_pipe = Pipeline([
    ('cv', CountVectorizer()),
    ('mnb', MultinomialNB())
])
test_pipeline(train_df, unigram_pipe, "Unigrams only")

# Unigrams only kfolds log losses: ['0.455', '0.46', '0.47', '0.473', '0.474']
# Unigrams only mean log loss: 0.466

# In[ ]:
tfidf_pipe = Pipeline([
    ('tfidf', TfidfVectorizer(min_df=3,
            strip_accents='unicode', analyzer='word', token_pattern=r'\w{1,}',
            ngram_range=(1, 3), sublinear_tf=True,
            stop_words = 'english')),
    ('clf', LogisticRegression(C=1.0))
])
test_pipeline(train_df, tfidf_pipe, "Tf-idf logistic regression, tri-grams")
# Tf-idf logistic regression, tri-grams kfolds log losses: ['0.625', '0.626', '0.63', '0.632', '0.635']
# Tf-idf logistic regression, tri-grams mean log loss: 0.63

# In[ ]:
tfidf_pipe = Pipeline([
    ('tfidf', TfidfVectorizer(min_df=3, max_df=0.95,
            strip_accents='unicode', analyzer='word',
            ngram_range=(1, 1), sublinear_tf=True,
            stop_words = 'english')),
    ('mnb', MultinomialNB())
])
test_pipeline(train_df, tfidf_pipe, "Tf-idf naive bayes unigrams")


# In[ ]:

# Number of stopwords in the text
eng_stopwords = set(stopwords.words("english"))
train_df["num_stopwords"] = train_df["text"].apply(lambda x: len([w for w in str(x).lower().split() if w in eng_stopwords]))
test_df["num_stopwords"] = test_df["text"].apply(lambda x: len([w for w in str(x).lower().split() if w in eng_stopwords]))
train_df.head()

# In[ ]:

# Line length
train_df["line_length"] = [len(line) for line in train_df['text']]
test_df["line_length"] = [len(line) for line in test_df['text']]
train_df.head()

# In[ ]:
unigram_pipe = Pipeline([
    ('cv', CountVectorizer()),
    ('mnb', MultinomialNB())
])
unigram_pipe.fit(train_df.text, train_df.author)
unigram_pipe.predict_proba(train_df.text)

unigram_predictions = pd.DataFrame(
        unigram_pipe.predict_proba(train_df.text),
        columns=['naive_bayes_pred_' + x for x in unigram_pipe.classes_])
unigram_predictions.head()

# In[ ]:
train_df = train_df.merge(unigram_predictions, left_index=True, right_index=True)
train_df.head()

# In[ ]:

df = train_df.drop(['text', 'author'], axis=1)
log_res = LogisticRegression()
log_res.fit(df, train_df.author)


unigram_test_predictions = pd.DataFrame(
        unigram_pipe.predict_proba(test_df.text),
        columns=['naive_bayes_pred_' + x for x in unigram_pipe.classes_])
unigram_test_predictions.head()
#df.head()
df = test_df.drop('text', axis=1)
df = df.merge(unigram_test_predictions, left_index=True, right_index=True)

y = log_res.predict_proba(df)
print(metrics.classification_report(y, log_res.classes_))

# In[ ]:
pred_df = pd.DataFrame(log_res.predict_proba(df),
                       columns=log_res.classes_)
pred_df.insert(0, column='id', value=test_df['id'])
pred_df.head()
pred_df.to_csv("submission.csv", index=False)